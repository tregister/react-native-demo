import React, {Component} from 'react';
import {Platform, View, Text, StyleSheet, Image, ScrollView, TextInput, Button, TouchableHighlight} from 'react-native'
import RCTNetworking from 'RCTNetworking';
class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {assignmentData: '', variationGroup: '', customVarColor: "#fff", loadingImage: 150};
    }

    componentDidMount() {
      this.getAssignmentData();
    }


    getAssignmentData = () => {
      console.log("Fetching Assignment Data...")
    const that = this;
    RCTNetworking.clearCookies(() => {});
    fetch('http://www.jslint.com').then(function(data) {console.log("JS LINT RETURNED");})
    fetch('http://examples-admin.sitespect.net:2503/__ssobj/api')
          .then((res) => res.json())
          .then(function(data) {
            console.log(data);
            console.log("Engine API RETURNED");
            var campaignColorV1 = data['1213958'];
            var campaignColorV2 = data['1213959'];
            var campaignColorControl = data['1213957'];

            // Variation Group 1
            if(typeof campaignColorV1 !== 'undefined'){
                campaignColorV1 = "" + campaignColorV1.VariationGroup_LiveVariables.color + "";
                that.setState({customVarColor: campaignColorV1});
                that.setState({variationGroup: 'Variation Group ID #1213958'});
            }
            // Variation Group ID: 1213959
            else if(typeof campaignColorV2 !== 'undefined'){
              campaignColorV2 = "" + campaignColorV2.VariationGroup_LiveVariables.color + "";
              that.setState({customVarColor: campaignColorV2});
              that.setState({variationGroup: 'Variation Group ID #1213959'});
            }
            // Control Group
            else if(typeof campaignColorControl !== 'undefined'){
              that.setState({customVarColor: "#ff0707"});
              that.setState({variationGroup: 'Control Group #1213957'});
            }

            // Total Assignment Data Returned
            var assignmentDataString = "" + JSON.stringify(data) + "";
            that.setState({assignmentData: assignmentDataString});
          })
    }


    render() {
      if (this.state.customVarColor !== "#fff") {
            this.state.loadingImage = 0;
        }
        return (
          <View>
                <Text style={{paddingLeft: 25, paddingTop: 25, }}>Custom Variable returned</Text>
                <View style={{alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{height: this.state.loadingImage, width: this.state.loadingImage}} source={require('../images/loading.gif')} />
                </View>
                <Text style={{paddingLeft: 25, color: this.state.customVarColor, fontSize: 32}}>{this.state.customVarColor}</Text>
                <View style={{paddingTop: 10, width: 200, alignItems: 'center', justifyContent: 'center', paddingTop: 10}}>
                <Button style={{alignItems: 'center', justifyContent: 'center'}} onPress={this.getAssignmentData} title="Get Assignments" color="green" accessibilityLabel="Get Assignments"/>
                </View>
                <Text style={{paddingTop: 10, paddingLeft: 25, paddingBottom: 10}}>{this.state.variationGroup}</Text>
                <Text style={{paddingLeft: 25}}>{this.state.assignmentData}</Text>
          </View>
        );
    }
}
export default HomeScreen;
